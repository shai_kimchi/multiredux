import React from 'react'
import { useSelector } from 'react-redux';
import _ from 'lodash'

export default function OverallContainer() {
    const states = useSelector(state => state || {});
    return (
        <div>
            <label>Redux's store keys</label>
            <ul>
                {_.map(_.keys(states), (key) => <li key={key}>{key}</li>)}
            </ul>
        </div>
    )
}
