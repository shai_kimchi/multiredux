import React, { useEffect } from 'react'
import store from '../../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import placesContainerReducer, { Select as setSelected } from '../../redux/placesContainer';

export default function PlacesContainer() {

    const data = useSelector(state => state?.places?.data || []);
    const dispatch = useDispatch()

    useEffect(() => {
        //component will mount
        store.injectReducer('placesContainer', placesContainerReducer)
        return () => {
            console.log('remove placesContainer reducer')
            store.removeReducer('placesContainer');
        }
    }, [])

    return (
        <div style={{ backgroundColor: 'lightblue' }}>
            <label>Places</label>
            <ul>
                {data.map(({ name, id }, i) => <li key={name + i} onClick={() => {
                    dispatch(setSelected(id))
                }}>{name}</li>)}
            </ul>
        </div>
    )
}
