import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import namesContainer from '../../redux/namesContainer';

export default function HeaderSummaryContainer({ onSelected }) {
    const namesCount = useSelector(state => state.names?.data?.length);
    const placesCount = useSelector(state => state.places?.data?.length);
    const namesSelected = useSelector(state => state.namesContainer?.selected);
    const placesSelected = useSelector(state => state.placesContainer?.selected);
    const [namesChecked, setNamesChecked] = useState(false)
    const [placesChecked, setPlacesChecked] = useState(false)

    return (
        <ul style={{ display: 'flex', width: '300px', justifyContent: 'space-around' }}>
            <li>
                <input
                    name="names"
                    type="checkbox"
                    checked={namesChecked}
                    onChange={() => {
                        setNamesChecked(!namesChecked)
                        onSelected({ names: !namesChecked, places: placesChecked })
                    }} />
                    Names: {namesCount} {namesSelected && ('- ' + namesSelected)}
            </li>
            <li>
                <input
                    name="places"
                    type="checkbox"
                    checked={placesChecked}
                    onChange={() => {
                        setPlacesChecked(!placesChecked)
                        onSelected({ names: namesChecked, places: !placesChecked })
                    }} />
                    Places: {placesCount} {placesSelected && ('- ' + placesSelected)}
            </li>
        </ul>
    )
}

HeaderSummaryContainer.defaultProps = {
    onSelected: () => { }
}
