import React, { useEffect } from 'react'
import store from '../../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import namesContainerReducer, { Select as setSelected } from '../../redux/namesContainer';

export default function NamesContainer() {

    const data = useSelector(state => state?.names?.data || []);
    const dispatch = useDispatch()

    useEffect(() => {
        //component will mount
        store.injectReducer('namesContainer', namesContainerReducer)
        return () => {
            console.log('remove namesContainer reducer')
            store.removeReducer('namesContainer');
        }
    }, [])

    return (
        <div style={{ backgroundColor: 'lightgrey' }}>
            <label>Names</label>
            <ul>
                {data.map(({ id, name }, i) => <li key={name + i} onClick={() => {
                    dispatch(setSelected(id))
                }}>{name}</li>)}
            </ul>
        </div>
    )
}
