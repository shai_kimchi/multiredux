import { createStore } from "redux";
import { combineReducers } from 'redux';

// import main from "./reducers";

// const staticReducers = {
//     main //needs default reducer
// }

function createReducer(asyncReducers) {
    console.log(asyncReducers)
    if (asyncReducers)
        return combineReducers({
            // ...staticReducers,
            ...asyncReducers
        })
    else
        return combineReducers({})
}

const store = createStore(
    createReducer(),
    {},
    window.devToolsExtension && window.devToolsExtension()
);

store.asyncReducers = {}

store.injectReducer = (key, asyncReducer) => {
    store.asyncReducers[key] = asyncReducer
    store.replaceReducer(createReducer(store.asyncReducers))
}

store.removeReducer = (key) => {
    delete store.asyncReducers[key];
    store.replaceReducer(createReducer(store.asyncReducers))
}

export default store;