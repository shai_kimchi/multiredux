import { combineReducers } from "redux";

const SELECT = 'places-container:select'
export const Select = (payload) => {
    return {
        type: SELECT,
        payload
    }
}

export const Reducer = (state = null, { type, payload }) => {
    switch (type) {
        case SELECT:
            return payload
        default:
            return state;            
    }
}

export const reducers = {
    selected: Reducer,
}

export default combineReducers(reducers)