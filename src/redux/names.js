import { combineReducers } from "redux";

const SET = 'names:set'
export const Set = (payload) => {
    return {
        type: SET,
        payload
    }
}

export const Reducer = (state = null, { type, payload }) => {
    switch (type) {
        case SET:
            return payload
        default:
            return state;            
    }
}

export const reducers = {
    data: Reducer,
}

export default combineReducers(reducers)