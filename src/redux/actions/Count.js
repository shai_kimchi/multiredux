export const INCREMENT = "inc";
export const DECREMENT = "dec";
export const SET = "set";
export const Inc = (payload) => {
  return {
    type: INCREMENT,
    payload
  }
}

export const Dec = (payload) => {
  return {
    type: DECREMENT,
    payload
  }
}

export const Set = (payload) => {  
  return {
    type: SET,
    payload
  }
}