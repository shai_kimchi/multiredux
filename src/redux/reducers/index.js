import { combineReducers } from "redux";
// import OnCountChage from './OnCountChange'
// import { INCREMENT, DECREMENT, SET } from '../actions/Count'
const SET = 'view:set'
export const Set = (payload) => {
    return {
        type: SET,
        payload
    }
}

export const OnViewChanged = (state = null, { type, payload }) => {
    switch (type) {
        case SET:
            return payload
        default:
            return state;            
    }
}

export const reducers = {
    view: OnViewChanged,
}

export default combineReducers(reducers)