import { INCREMENT, DECREMENT, SET } from '../actions/Count'
export default (state = 0, { type, payload }) => {
    switch (type) {
        case INCREMENT:
            return state + 1
        case DECREMENT:
            return state - 1
        case SET:
            return payload
        default:
            return state
    }
}
