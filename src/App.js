import React, { useEffect, useState } from 'react';
import './App.css';
import { useDispatch } from 'react-redux'

import namesData from './data/names.json';
import namesReducer, { Set as setNames } from "./redux/names"

import placesData from './data/places.json';
import placesReducer, { Set as setPlaces } from "./redux/places"

import store from './redux/store';
import HeaderSummaryContainer from './containers/HeaderSummaryContainer.js/HeaderSummaryContainer';

import NamesContainer from './containers/NamesContainer/NamesContainer';
import PlacesContainer from './containers/PlacesContainer/PlacesContainer';

import style from './Style.module.css';
import OverallContainer from './containers/OverallContainer/OverallContainer';

function App() {

  const dispatch = useDispatch()
  useEffect(() => {
    //component will mount
    store.injectReducer('names', namesReducer)
    dispatch(setNames(namesData));

    store.injectReducer('places', placesReducer)
    dispatch(setPlaces(placesData));

    return () => {
      console.log('App unmounts clear all reducers')
    }
  }, [])

  const [selections, setSelections] = useState({})

  const handleHeaderSelections = (headerSelections) => {
    setSelections({ ...headerSelections });
  }

  return (
    <div className="App">
      <div className={style.header}>
        <HeaderSummaryContainer onSelected={handleHeaderSelections} />
      </div>
      <div className={style.sections}>
        {selections.names && <NamesContainer />}
        {selections.places && <PlacesContainer />}
        <OverallContainer />
      </div>
    </div>
  );
}

export default App;
