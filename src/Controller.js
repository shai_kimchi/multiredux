import React, { useState } from 'react'

export default function Controller(props) {
    // Declare a new state variable, which we'll call "count"
    const [count, setCount] = useState(0);
    const propCount = props.count || 0;
    const onChange = props.onChange || (() => { })

    const incCount = (by) => {
        onChange(count + by)
        setCount(count + by);
    }

    return (
        <div>
            <button onClick={() => { incCount(1) }}>+</button>
            <button onClick={() => { incCount(-1) }}>-</button>
            <span>|</span>
            <span>{count}</span>
            <span>|</span>
            <span>{propCount}</span>
            <span>|</span>
            <span>{props.children}</span>
        </div>
    )
}
